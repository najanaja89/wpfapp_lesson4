﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp_lesson4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            image.Source = new BitmapImage(new Uri("https://vignette.wikia.nocookie.net/suzumiyaharuhi/images/8/80/%D0%90%D1%81%D0%B0%D1%85%D0%B8%D0%BD%D0%B0_%D0%BC%D0%B8%D0%BA%D1%83%D1%80%D1%83.jpg/revision/latest?cb=20130110094510&path-prefix=ru"));

            listBox.ItemsSource = new List<Book>
            {
                new Book
                {
                    Id=1,
                    Name="Hyperion",
                    Price=1000
                },
            new Book
            {
                Id = 1,
                Name = "Gods Themselvs",
                Price = 10000
            }
            };
        }
    }
}
